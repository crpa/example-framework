package com.example.starter.mongodb.configure;

import org.springframework.data.mongodb.config.EnableMongoAuditing;

/**
 * @author 王令
 * @since 2023/5/10
 */
@EnableMongoAuditing
public class MongoDBConfiguration {

}
