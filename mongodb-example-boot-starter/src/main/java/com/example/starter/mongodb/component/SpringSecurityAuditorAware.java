package com.example.starter.mongodb.component;

import com.example.starter.security.util.SecurityUtil;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * @author 王令
 * @since 2023/5/10
 */
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(SecurityUtil.getUsername());
    }
}
