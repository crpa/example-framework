package com.example.starter.amazon.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2022/11/6 9:24
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "com.example.amazons3")
public class AmazonS3Properties {

    private String endpoint = "http://127.0.0.1:9000";
    private String accessKey = "admin";
    private String secretKey = "admin123";
    private String bucket = "public";

}
