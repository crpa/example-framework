package com.example.starter.amazon.rule;

/**
 * @author 王令
 * @since 2023/1/7
 */
public interface OssRule {

    String getKey(String filename);

    String getDir();
}
