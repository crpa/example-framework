package com.example.starter.amazon.pojo;

import lombok.Data;

/**
 * @author 王令
 * @since 2023/1/9
 */
@Data
public class OssResponse {

    private String endpoint;
    private String bucketName;
    private String key;
    private String filename;
    private String url;
}
