package com.example.starter.log.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/4/17 10:38
 */
@Data
@ConfigurationProperties(prefix = "com.example.log.api")
public class ApiLogProperties {

    private boolean enabled = true;
}
