package com.example.starter.log.publisher;

import cn.hutool.extra.spring.SpringUtil;
import com.example.starter.log.pojo.ApiLog;
import com.example.starter.security.util.SecurityUtil;
import com.example.starter.webmvc.util.WebUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 王令
 * @since 2023/4/17 9:25
 */
public class ApiLogPublisher {

    public static void publishEvent(String module, String notes, String code, long time) {
        HttpServletRequest request = WebUtil.getRequest();
        ApiLog log = new ApiLog();
        log.setMethod(request.getMethod());
        log.setIpAddr(WebUtil.getIp(request));
        log.setPath(request.getServletPath());
        log.setQueryString(request.getQueryString());
        log.setModule(module);
        log.setNotes(notes);
        log.setCode(code);
        log.setTime(time);
        log.setUsername(SecurityUtil.getUsername());
        SpringUtil.publishEvent(log);
    }

}
