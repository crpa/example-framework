package com.example.starter.log.pojo;

import lombok.Data;

/**
 * @author 王令
 * @since 2023/4/17 9:18
 */
@Data
public class ApiLog {

    private String method;
    private String ipAddr;
    private String path;
    private String queryString;
    private String module;
    private String notes;
    private String code;
    private long time;
    private String username;

}
