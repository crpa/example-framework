package com.example.starter.log.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 王令
 * @since 2023/4/14 15:50
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ApiLog {


    /**
     * @return 模块名
     */
    String module() default "";

    /**
     * @return 描述
     */
    @AliasFor("notes")
    String value() default "日志记录";

    /**
     * @return 描述
     */
    @AliasFor("value")
    String notes() default "日志记录";

    /**
     * @return 没有实际意义，需结合业务定义
     */
    String code() default "";


}
