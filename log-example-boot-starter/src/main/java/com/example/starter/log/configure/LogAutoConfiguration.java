package com.example.starter.log.configure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author 王令
 * @since 2023/4/14 16:38
 */
@EnableConfigurationProperties(ApiLogProperties.class)
public class LogAutoConfiguration {

}
