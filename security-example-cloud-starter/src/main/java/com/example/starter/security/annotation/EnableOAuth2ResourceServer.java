package com.example.starter.security.annotation;

import com.example.starter.security.configure.OAuth2ResourceConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 王令
 * @since 2022/8/9 21:08
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ImportAutoConfiguration({
        OAuth2ResourceConfiguration.class
})
public @interface EnableOAuth2ResourceServer {
}
