package com.example.starter.security.configure;

import com.example.starter.security.component.ExampleRedisTokenStore;
import com.example.starter.security.component.PermissionService;
import com.example.starter.security.handler.SecurityExceptionHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;


/**
 * @author 王令
 * @since 2022/8/7 18:42
 */
@Configuration
@EnableConfigurationProperties({SecurityProperties.class})
@ImportAutoConfiguration(SecurityExceptionHandler.class)
@RequiredArgsConstructor
public class SecurityAutoConfiguration {

    private final SecurityProperties properties;

    @Bean
    public TokenStore tokenStore(RedisConnectionFactory factory) {
        ExampleRedisTokenStore tokenStore = new ExampleRedisTokenStore(factory);
        tokenStore.setPrefix(properties.getPrefix());
        return tokenStore;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean("pm")
    public PermissionService permissionService() {
        return new PermissionService();
    }

}
