package com.example.starter.security.component;

import com.example.starter.webmvc.util.WebUtil;
import lombok.RequiredArgsConstructor;

/**
 * @author 王令
 * @since 2022/9/18 15:22
 */
@RequiredArgsConstructor
public class PermissionService {

    /**
     * 判断是否为内部请求(Feign)
     */
    public boolean isInner() {
        return WebUtil.isInner();
    }

    public boolean hasHeader(String name) {
        return WebUtil.hasHeader(name);
    }

    public boolean hasHeader(String name,String value) {
        return WebUtil.hasHeader(name, value);
    }
}
