package com.example.starter.security.handler;

import com.example.starter.security.constant.SecurityExceptionEnum;
import com.example.starter.util.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 统一异常处理
 * </p>
 *
 * @author 王令
 * @since 2022-05-13 15:14:14
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
@RestControllerAdvice
public class SecurityExceptionHandler {

    private final HttpServletRequest request;

    @ExceptionHandler(UsernameNotFoundException.class)
    public Response<String> usernameNotFound(Exception e) {
        printLog(e);
        return Response.fail(SecurityExceptionEnum.USERNAME_NOT_FOUND);
    }

    @ExceptionHandler(InvalidGrantException.class)
    public Response<String> invalidGrant(Exception e) {
        printLog(e);
        return Response.fail(SecurityExceptionEnum.INVALID_GRANT);
    }

    @ExceptionHandler(InvalidTokenException.class)
    public Response<String> invalidToken(Exception e) {
        printLog(e);
        return Response.fail(SecurityExceptionEnum.INVALID_TOKEN);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public Response<String> accessDenied(Exception e) {
        printLog(e);
        return Response.fail(SecurityExceptionEnum.ACCESS_DENIED);
    }

    @ExceptionHandler
    public Response<String> badCredentialsException(BadCredentialsException e) {
        printLog(e);
        return Response.fail(SecurityExceptionEnum.INVALID_GRANT.getCode(), e.getLocalizedMessage());
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    public Response<String> internalAuthenticationServiceError(Exception e) {
        printLog(e);
        return Response.error(SecurityExceptionEnum.INTERNAL_AUTHENTICATION_ERROR);
    }


    private void printLog(Exception e) {
        log.error("======== start ========");
        log.error("remoteAddr: {}", request.getRemoteAddr());
        log.error("errMsg: {}", e.getLocalizedMessage());
        log.error("token: {}", request.getHeader(HttpHeaders.AUTHORIZATION));
        log.error("======== end ========");
    }
}
