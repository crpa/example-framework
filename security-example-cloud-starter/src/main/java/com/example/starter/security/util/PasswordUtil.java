package com.example.starter.security.util;

import cn.hutool.extra.spring.SpringUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * <p>
 * 密码编码校验工具
 * </p>
 *
 * @author 王令
 * @since 2022/7/16 12:37
 */
@UtilityClass
public class PasswordUtil {

    public PasswordEncoder passwordEncoder() {
        return SpringUtil.getBean(PasswordEncoder.class);
    }

    public String encode(CharSequence rawPassword) {
        return passwordEncoder().encode(rawPassword);
    }

    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return passwordEncoder().matches(rawPassword, encodedPassword);
    }

    public boolean upgradeEncoding(String encodedPassword) {
        return passwordEncoder().upgradeEncoding(encodedPassword);
    }
}
