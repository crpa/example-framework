package com.example.starter.security.constant;

import com.example.starter.constant.ResultStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 王令
 * @since 2022/9/13 17:27
 */
@RequiredArgsConstructor
@Getter
public enum SecurityExceptionEnum implements ResultStatus {
    USERNAME_NOT_FOUND(40100, "用户不存在"),
    INVALID_GRANT(40100, "认证失败"),
    INVALID_TOKEN(40101, "令牌无效或未携带令牌"),
    ACCESS_DENIED(40300, "没有权限，不允许访问"),
    INTERNAL_AUTHENTICATION_ERROR(50301, "服务异常，无法处理身份验证请求"),
    ;

    private final Integer code;
    private final String message;
}
