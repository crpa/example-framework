package com.example.starter.pubsub.api;

/**
 * @author 王令
 * @since 2023/6/30 14:57
 */
public interface SubListener<M> {

    void onMessage(M message);
}
