package com.example.starter.pubsub.kafka;

import com.example.starter.pubsub.api.PubResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.kafka.support.SendResult;

/**
 * @author 王令
 * @since 2023/6/30 11:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaPubResultAdapter<M> implements PubResult {

    private SendResult<String, M> sendResult;

}
