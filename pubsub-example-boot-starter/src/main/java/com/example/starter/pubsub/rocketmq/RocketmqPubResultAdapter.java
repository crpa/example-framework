package com.example.starter.pubsub.rocketmq;

import com.example.starter.pubsub.api.PubResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.rocketmq.client.producer.SendResult;

/**
 * @author 王令
 * @since 2023/6/30 11:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RocketmqPubResultAdapter implements PubResult {

    private SendResult sendResult;
}
