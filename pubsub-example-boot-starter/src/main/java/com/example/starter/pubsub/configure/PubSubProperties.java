package com.example.starter.pubsub.configure;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2023/6/30 11:31
 */
@Data
@ConfigurationProperties(prefix = "com.example.pub-sub")
public class PubSubProperties {

    // event / redis / rocketmq / kafka
    private String type = "event";

}
