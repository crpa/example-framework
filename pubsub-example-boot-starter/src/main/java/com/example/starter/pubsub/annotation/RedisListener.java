package com.example.starter.pubsub.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

/**
 * @author 王令
 * @since 2023/6/30 16:09
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RedisListener {

    /**
     * subscribe for topic，property place holders ${...} are supported.
     */
    @AliasFor("topic")
    String value() default "${spring.redis.topic}";

    /**
     * subscribe for topic，property place holders ${...} are supported.
     */
    @AliasFor("value")
    String topic() default "${spring.redis.topic}";
}
