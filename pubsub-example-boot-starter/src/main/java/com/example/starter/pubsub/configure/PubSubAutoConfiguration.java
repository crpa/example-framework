package com.example.starter.pubsub.configure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * @author 王令
 * @since 2023/6/30 11:36
 */
@EnableConfigurationProperties(PubSubProperties.class)
public class PubSubAutoConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "com.example.pub-sub", name = "type", havingValue = "redis")
    public RedisMessageListenerContainer container(RedisConnectionFactory factory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        return container;
    }
}
