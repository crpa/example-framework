package com.example.starter.pubsub.api;

/**
 * @author 王令
 * @since 2023/6/30 10:48
 */
public interface PubCallback {

    void onSuccess(PubResult pubResult);

    void onFailure(Throwable ex);
}
