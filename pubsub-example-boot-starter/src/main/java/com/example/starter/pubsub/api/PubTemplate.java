package com.example.starter.pubsub.api;

/**
 * @author 王令
 * @since 2023/6/30 10:41
 */
public interface PubTemplate {

    /**
     * 同步响应
     * @param topic 主题
     * @param message 消息
     * @return 响应
     * @throws Exception 异常
     */
    PubResult syncSend(String topic, Object message) throws Exception;

    /**
     * 异步回调
     * @param topic 主题
     * @param message 消息
     * @param callback 回调
     */
    void asyncSend(String topic, Object message, PubCallback callback);

    /**
     * 异步不回调
     * @param topic 主题
     * @param message 消息
     */
    void asyncSend(String topic, Object message);
}
