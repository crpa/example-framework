package com.example.starter.mybatis.util;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDto;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Pageable;


/**
 * @author 王令
 * @since 2022/8/14 15:16
 */
@UtilityClass
public class PageUtil {

    public <T> IPage<T> getPage(Pageable pageable) {
        PageDto<T> page = new PageDto<>(pageable.getPageNumber() + 1, pageable.getPageSize());
        pageable.getSort().forEach(sort -> {
            if (sort.isAscending()) {
                page.addOrder(OrderItem.asc(sort.getProperty()));
            } else {
                page.addOrder(OrderItem.desc(sort.getProperty()));
            }
        });
        return page;
    }

}
