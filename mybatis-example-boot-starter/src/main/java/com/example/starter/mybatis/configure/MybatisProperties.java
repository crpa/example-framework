package com.example.starter.mybatis.configure;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2022/10/3 8:45
 */
@Setter
@Getter
@ConfigurationProperties("com.example.mybatis")
public class MybatisProperties {

    private MultiTenant multiTenant = new MultiTenant();

    @Getter
    @Setter
    public static class MultiTenant {
        private Boolean enabled = true;
        private String basePackage;
        private String master = "e_cloud";
    }

    public boolean isMaster(String tenantId) {
        return StrUtil.equals(multiTenant.master, tenantId);
    }

    public String getMaster() {
        return multiTenant.master;
    }

}
