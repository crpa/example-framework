package com.example.starter.mybatis.base;

import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author 王令
 * @since 2023/3/3 11:39
 */
public interface BaseEntityMapper<T extends BaseEntity> extends BaseMapper<T> {

    default T selectFirst(LambdaQueryWrapper<T> wrapper) {
        return this.selectOne(wrapper.last("limit 1"));
    }

    default boolean exists(LambdaQueryWrapper<T> wrapper) {
        T t = this.selectFirst(wrapper.select(T::getId));
        return ObjUtil.isNotNull(t);
    }
}
