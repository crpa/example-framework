package com.example.starter.mybatis.base;

import com.baomidou.mybatisplus.annotation.*;
import com.example.starter.mybatis.anntation.CreateTime;
import com.example.starter.mybatis.anntation.DeleteTime;
import com.example.starter.mybatis.anntation.UpdateTime;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * <p>
 *     mybatis 抽象实体
 * </p>
 * @author 王令
 * @since 2022-05-13 12:40:40
 */
@Getter
@Setter
public abstract class BaseEntity {

    /**
     * 主键
     */
    @EqualsAndHashCode.Include
    @ToString.Include
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    @CreateTime
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @UpdateTime
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 逻辑删除
     */
    @JsonIgnore
    @TableLogic
    @TableField(select = false)
    private Boolean deleted;

    /**
     * 删除时间
     */
    @JsonIgnore
    @DeleteTime
    @TableField(fill = FieldFill.INSERT, select = false)
    private LocalDateTime deleteTime;
}
