package com.example.starter.mybatis.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author 王令
 * @since 2022/10/3 9:16
 */
@Getter
@Setter
public abstract class MultiTenantEntity extends BaseEntity implements MultiTenant {

    /**
     * 租户id
     */
    @EqualsAndHashCode.Include
    private String tenantId;
}
