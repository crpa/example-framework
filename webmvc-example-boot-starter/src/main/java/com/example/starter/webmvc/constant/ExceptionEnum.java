package com.example.starter.webmvc.constant;

import com.example.starter.constant.ResultStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author 王令
 * @since 2022/9/21 17:05
 */
@Getter
@RequiredArgsConstructor
public enum ExceptionEnum implements ResultStatus {
    BIND_EXCEPTION(40010, "参数校验错误"),
    NOT_FOUND_404(40400, "请求地址不存在"),
    METHOD_NOT_ALLOWED(40500, "请求方法不允许"),
    SYSTEM_ERROR(50000, "服务器异常")
    ;
    private final Integer code;
    private final String message;
}
