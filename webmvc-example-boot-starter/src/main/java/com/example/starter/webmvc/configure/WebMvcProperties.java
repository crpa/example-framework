package com.example.starter.webmvc.configure;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 王令
 * @since 2022/8/3 13:40
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "com.example.mvc")
public class WebMvcProperties {

    private I18n i18n = new I18n();

    private Error error = new Error();

    @Getter
    @Setter
    public static class I18n {
        private Boolean enabled = false;
        private String basename = "i18n/messages";
    }

    @Getter
    @Setter
    public static class Error {
        private Boolean throwable = false;
        private String errMsg = "服务器异常";
    }
}
