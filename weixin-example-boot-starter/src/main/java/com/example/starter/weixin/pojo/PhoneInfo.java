package com.example.starter.weixin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class PhoneInfo {
    private String phoneNumber;
    private String purePhoneNumber;
    private String countryCode;
    @JsonIgnore
    private Watermark watermark;
}

@Data
class Watermark {
    private Long timestamp;
    private String appid;
}
