package com.example.starter.weixin.pojo;

import lombok.Data;

/**
 * @author 王令
 * @since 2023/4/9
 */
@Data
public class WxPhone {

    private Integer errcode;
    private String errmsg;
    private PhoneInfo phone_info;
}
