package com.example.starter.weixin.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * @author 王令
 * @since 2023/4/9
 */
@Data
public class WxSession {
    private String session_key;
    private String unionid;
    private String openid;
    @JsonIgnore
    private String errmsg;
    @JsonIgnore
    private Integer errcode;
}
