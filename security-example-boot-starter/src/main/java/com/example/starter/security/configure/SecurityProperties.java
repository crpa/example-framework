package com.example.starter.security.configure;

import cn.hutool.core.collection.ListUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author 王令
 * @since 2022/8/7 18:49
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "com.example.security")
public class SecurityProperties {

    private List<String> permitUrls = ListUtil.of();

    private String prefix = "ec:";

}
