package com.example.starter.security.pojo;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ObjUtil;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 王令
 * @since 2022/7/30 17:41
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ExampleUser implements UserDetails {


    private static final long serialVersionUID = 7134920883290498203L;

    private Long id;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 锁定状态 true 未锁定 false 锁定
     */
    private Boolean nonLocked;

    /**
     * 账户状态
     */
    private Boolean enabled;

    /**
     * 角色列表
     */
    private List<String> roles;

    /**
     * 权限列表
     */
    private List<String> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (ObjUtil.isEmpty(authorities)) {
            return ListUtil.empty();
        }
        return authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return nonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
