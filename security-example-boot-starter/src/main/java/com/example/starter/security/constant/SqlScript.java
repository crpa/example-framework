package com.example.starter.security.constant;

/**
 * @author 王令
 * @since 2022/12/26 16:26
 */
public interface SqlScript {

    String CLIENT_FIELDS_FOR_UPDATE = "resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    String CLIENT_FIELDS = "client_secret, " + CLIENT_FIELDS_FOR_UPDATE;

    String BASE_FIND_STATEMENT = "select client_id, " + CLIENT_FIELDS
            + " from sys_oauth_client_details";

    String FIND_STATEMENT = BASE_FIND_STATEMENT + " order by client_id";

    String SELECT_STATEMENT = BASE_FIND_STATEMENT + " where client_id = ?";

    String INSERT_STATEMENT = "insert into sys_oauth_client_details (" + CLIENT_FIELDS
            + ", client_id) values (?,?,?,?,?,?,?,?,?,?,?)";

    String UPDATE_STATEMENT = "update sys_oauth_client_details " + "set "
            + CLIENT_FIELDS_FOR_UPDATE.replaceAll(", ", "=?, ") + "=? where client_id = ?";

    String UPDATE_SECRET_STATEMENT = "update sys_oauth_client_details "
            + "set client_secret = ? where client_id = ?";

    String DELETE_STATEMENT = "delete from sys_oauth_client_details where client_id = ?";


}
