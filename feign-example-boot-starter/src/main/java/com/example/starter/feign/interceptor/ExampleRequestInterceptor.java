package com.example.starter.feign.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletRequest;

import static com.example.starter.constant.SecurityConstant.AUTHORIZATION;
import static com.example.starter.constant.WebConstant.*;

/**
 * @author 王令
 * @since 2022/9/11 7:01
 */
@RequiredArgsConstructor
public class ExampleRequestInterceptor implements RequestInterceptor {

    private final HttpServletRequest request;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header(FROM, FROM_INNER);
        requestTemplate.header(AUTHORIZATION, request.getHeader(AUTHORIZATION));
        requestTemplate.header(TENANT, request.getHeader(TENANT));
    }

}
