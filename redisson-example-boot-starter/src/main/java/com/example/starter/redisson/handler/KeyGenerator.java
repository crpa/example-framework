package com.example.starter.redisson.handler;

import com.example.starter.redisson.anntation.RedissonLock;
import org.aspectj.lang.JoinPoint;

/**
 * @author 王令
 * @since 2022/11/23 14:28
 */
@FunctionalInterface
public interface KeyGenerator {

    String generate(JoinPoint point, RedissonLock lock);
}
