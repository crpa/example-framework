package com.example.starter.redisson.configure;

import lombok.RequiredArgsConstructor;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author 王令
 * @since 2022/11/8 21:09
 */
@EnableConfigurationProperties(RedissonProperties.class)
@RequiredArgsConstructor
public class RedissonAutoConfiguration {

    private final RedisProperties properties;

    @Bean
    @ConditionalOnMissingBean(RedissonClient.class)
    public RedissonClient singleServerRedissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://" + properties.getHost() + ":" + properties.getHost())
                .setUsername(properties.getUsername())
                .setPassword(properties.getPassword())
                .setDatabase(properties.getDatabase());
        return Redisson.create(config);
    }

}
