package com.example.starter.redisson.handler;

import com.example.starter.redisson.constant.LockMode;
import lombok.RequiredArgsConstructor;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

/**
 * @author 王令
 * @since 2022/11/23 14:24
 */
@RequiredArgsConstructor
public class ReadLockStrategy implements LockModeStrategy {

    private final RedissonClient client;

    @Override
    public boolean elected(LockMode mode) {
        return LockMode.READ_LOCK.equals(mode);
    }

    @Override
    public RLock getLock(String key) {
        return client.getReadWriteLock(key).readLock();
    }
}
