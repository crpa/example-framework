package com.example.starter.redisson.anntation;

import com.example.starter.redisson.constant.LockMode;
import com.example.starter.redisson.handler.KeyGenerator;
import com.example.starter.redisson.handler.SpELKeyGenerator;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author 王令
 * @since 2022/11/14 12:16
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface RedissonLock {

    /**
     * 默认使用方法名
     */
    String name() default "";

    /**
     * 默认使用参数列表，支持el表达式
     */
    String key() default "";

    /**
     * key生成策略
     */
    Class<? extends KeyGenerator> keyGenerator() default SpELKeyGenerator.class;

    /**
     * 锁类型
     */
    LockMode mode() default LockMode.REENTRANT_LOCK;

    // 最大等待时间
    long waitTime() default 30L;

    // 持有锁的最大时间
    long leaseTime() default 30L;

    // 时间单位
    TimeUnit timeUnit() default TimeUnit.SECONDS;

}
