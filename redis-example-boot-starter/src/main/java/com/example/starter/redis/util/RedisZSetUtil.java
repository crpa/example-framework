package com.example.starter.redis.util;

import org.springframework.data.redis.connection.RedisZSetCommands.Aggregate;
import org.springframework.data.redis.connection.RedisZSetCommands.Limit;
import org.springframework.data.redis.connection.RedisZSetCommands.Range;
import org.springframework.data.redis.connection.RedisZSetCommands.Weights;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;

import java.util.Collection;
import java.util.Set;

/**
 * @author 王令
 * @since 2023/3/20 9:40
 */
public class RedisZSetUtil {

    private static ZSetOperations<String, Object> operations;

    public RedisZSetUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisZSetUtil.operations = redisTemplate.opsForZSet();
    }

    public static Boolean add(String key, Object value, double score) {
        return operations.add(key, value, score);
    }

    public static Long add(String key, Set<TypedTuple<Object>> typedTuples) {
        return operations.add(key, typedTuples);
    }

    public static Long remove(String key, Object... values) {
        return operations.remove(key, values);
    }

    public static Double incrementScore(String key, Object value, double delta) {
        return operations.incrementScore(key, value, delta);
    }

    public static Long rank(String key, Object o) {
        return operations.rank(key, o);
    }

    public static Long reverseRank(String key, Object o) {
        return operations.reverseRank(key, o);
    }

    public static Set<Object> range(String key, long start, long end) {
        return operations.range(key, start, end);
    }

    public static Set<TypedTuple<Object>> rangeWithScores(String key, long start, long end) {
        return operations.rangeWithScores(key, start, end);
    }

    public static Set<Object> rangeByScore(String key, double min, double max) {
        return operations.rangeByScore(key, min, max);
    }

    public static Set<TypedTuple<Object>> rangeByScoreWithScores(String key, double min, double max) {
        return operations.rangeByScoreWithScores(key, min, max);
    }

    public static Set<Object> rangeByScore(String key, double min, double max, long offset, long count) {
        return operations.rangeByScore(key, min, max, offset, count);
    }

    public static Set<TypedTuple<Object>> rangeByScoreWithScores(String key, double min, double max, long offset, long count) {
        return operations.rangeByScoreWithScores(key, min, max, offset, count);
    }

    public static Set<Object> reverseRange(String key, long start, long end) {
        return operations.reverseRange(key, start, end);
    }

    public static Set<TypedTuple<Object>> reverseRangeWithScores(String key, long start, long end) {
        return operations.reverseRangeWithScores(key, start, end);
    }

    public static Set<Object> reverseRangeByScore(String key, double min, double max) {
        return operations.reverseRangeByScore(key, min, max);
    }

    public static Set<TypedTuple<Object>> reverseRangeByScoreWithScores(String key, double min, double max) {
        return operations.reverseRangeByScoreWithScores(key, min, max);
    }

    public static Set<Object> reverseRangeByScore(String key, double min, double max, long offset, long count) {
        return operations.reverseRangeByScore(key, min, max, offset, count);
    }

    public static Set<TypedTuple<Object>> reverseRangeByScoreWithScores(String key, double min, double max, long offset,
            long count) {
        return operations.reverseRangeByScoreWithScores(key, min, max, offset, count);
    }

    public static Long count(String key, double min, double max) {
        return operations.count(key, min, max);
    }

    public static Long lexCount(String key, Range range) {
        return operations.lexCount(key, range);
    }

    public static Long size(String key) {
        return operations.size(key);
    }

    public static Long zCard(String key) {
        return operations.zCard(key);
    }

    public static Double score(String key, Object o) {
        return operations.score(key, o);
    }

    public static Long removeRange(String key, long start, long end) {
        return operations.removeRange(key, start, end);
    }

    public static Long removeRangeByScore(String key, double min, double max) {
        return operations.removeRangeByScore(key, min, max);
    }

    public static Long unionAndStore(String key, String otherKey, String destKey) {
        return operations.unionAndStore(key, otherKey, destKey);
    }

    public static Long unionAndStore(String key, Collection<String> otherKeys, String destKey) {
        return operations.unionAndStore(key, otherKeys, destKey);
    }

    public static Long unionAndStore(String key, Collection<String> otherKeys, String destKey, Aggregate aggregate,
            Weights weights) {
        return operations.unionAndStore(key, otherKeys, destKey, aggregate, weights);
    }

    public static Long intersectAndStore(String key, String otherKey, String destKey) {
        return operations.intersectAndStore(key, otherKey, destKey);
    }

    public static Long intersectAndStore(String key, Collection<String> otherKeys, String destKey) {
        return operations.intersectAndStore(key, otherKeys, destKey);
    }

    public static Long intersectAndStore(String key, Collection<String> otherKeys, String destKey, Aggregate aggregate,
            Weights weights) {
        return operations.intersectAndStore(key, otherKeys, destKey, aggregate, weights);
    }

    public static Cursor<TypedTuple<Object>> scan(String key, ScanOptions options) {
        return operations.scan(key, options);
    }

    public static Set<Object> rangeByLex(String key, Range range, Limit limit) {
        return operations.rangeByLex(key, range, limit);
    }

    public static Set<Object> reverseRangeByLex(String key, Range range, Limit limit) {
        return operations.reverseRangeByLex(key, range, limit);
    }

    public static RedisOperations<String, Object> getOperations() {
        return operations.getOperations();
    }
}
