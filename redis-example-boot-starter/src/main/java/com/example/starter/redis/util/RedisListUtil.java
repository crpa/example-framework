package com.example.starter.redis.util;

import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 王令
 * @since 2023/3/20 9:30
 */
public class RedisListUtil {

    private static ListOperations<String, Object> operations;

    public RedisListUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisListUtil.operations = redisTemplate.opsForList();
    }

    public static List<Object> range(String key, long start, long end) {
        return operations.range(key, start, end);
    }

    public static void trim(String key, long start, long end) {
        operations.trim(key, start, end);
    }

    public static Long size(String key) {
        return operations.size(key);
    }

    public static Long leftPush(String key, Object value) {
        return operations.leftPush(key, value);
    }

    public static Long leftPushAll(String key, Object... values) {
        return operations.leftPushAll(key, values);
    }

    public static Long leftPushAll(String key, Collection<Object> values) {
        return operations.leftPushAll(key, values);
    }

    public static Long leftPushIfPresent(String key, Object value) {
        return operations.leftPushIfPresent(key, value);
    }

    public static Long leftPush(String key, Object pivot, Object value) {
        return operations.leftPush(key, pivot, value);
    }

    public static Long rightPush(String key, Object value) {
        return operations.rightPush(key, value);
    }

    public static Long rightPushAll(String key, Object... values) {
        return operations.rightPushAll(key, values);
    }

    public static Long rightPushAll(String key, Collection<Object> values) {
        return operations.rightPushAll(key, values);
    }

    public static Long rightPushIfPresent(String key, Object value) {
        return operations.rightPushIfPresent(key, value);
    }

    public static Long rightPush(String key, Object pivot, Object value) {
        return operations.rightPush(key, pivot, value);
    }

    public static void set(String key, long index, Object value) {
        operations.set(key, index, value);
    }

    public static Long remove(String key, long count, Object value) {
        return operations.remove(key, count, value);
    }

    public static Object index(String key, long index) {
        return operations.index(key, index);
    }

    public static Long indexOf(String key, Object value) {
        return operations.indexOf(key, value);
    }

    public static Long lastIndexOf(String key, Object value) {
        return operations.lastIndexOf(key, value);
    }

    public static Object leftPop(String key) {
        return operations.leftPop(key);
    }

    public static Object leftPop(String key, long timeout, TimeUnit unit) {
        return operations.leftPop(key, timeout, unit);
    }

    public static Object rightPop(String key) {
        return operations.rightPop(key);
    }

    public static Object rightPop(String key, long timeout, TimeUnit unit) {
        return operations.rightPop(key, timeout, unit);
    }

    public static Object rightPopAndLeftPush(String sourceKey, String destinationKey) {
        return operations.rightPopAndLeftPush(sourceKey, destinationKey);
    }

    public static Object rightPopAndLeftPush(String sourceKey, String destinationKey, long timeout, TimeUnit unit) {
        return operations.rightPopAndLeftPush(sourceKey, destinationKey, timeout, unit);
    }

    public static RedisOperations<String, Object> getOperations() {
        return operations.getOperations();
    }
}
