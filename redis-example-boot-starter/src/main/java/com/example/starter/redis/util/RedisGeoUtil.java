package com.example.starter.redis.util;

import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands.GeoLocation;
import org.springframework.data.redis.connection.RedisGeoCommands.GeoRadiusCommandArgs;
import org.springframework.data.redis.core.GeoOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

/**
 * @author 王令
 * @since 2023/5/17 16:39
 */
public class RedisGeoUtil {

    private static GeoOperations<String, Object> operations;

    public RedisGeoUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisGeoUtil.operations = redisTemplate.opsForGeo();
    }

    public static void add(String key, Point point, Object member) {
        operations.add(key, point, member);
    }

    public static Distance distance(String key, Object member1, Object member2) {
        return operations.distance(key, member1, member2);
    }

    public static Distance distance(String key, Object member1, Object member2, Metric metric) {
        return operations.distance(key, member1, member2, metric);
    }

    public static List<String> hash(String key, Object... members) {
        return operations.hash(key, members);
    }

    public static List<Point> position(String key, Object... members) {
        return operations.position(key, members);
    }

    public static GeoResults<GeoLocation<Object>> radius(String key, Circle within) {
        return operations.radius(key, within);
    }

    public static GeoResults<GeoLocation<Object>> radius(String key, Circle within, GeoRadiusCommandArgs args) {
        return operations.radius(key, within, args);
    }

    public static GeoResults<GeoLocation<Object>> radius(String key, Object member, double radius) {
        return operations.radius(key, member, radius);
    }

    public static GeoResults<GeoLocation<Object>> radius(String key, Object member, Distance distance) {
        return operations.radius(key, member, distance);
    }

    public static GeoResults<GeoLocation<Object>> radius(String key, Object member, Distance distance, GeoRadiusCommandArgs args) {
        return operations.radius(key, member, distance, args);
    }

    public static void remove(String key, Object... members) {
        operations.remove(key, members);
    }

}
