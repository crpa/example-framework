package com.example.starter.redis.util;

import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 王令
 * @since 2023/3/20 9:21
 */
public class RedisValueUtil {

    private static ValueOperations<String, Object> operations;

    public RedisValueUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisValueUtil.operations = redisTemplate.opsForValue();
    }

    public static void set(String key, Object value) {
        operations.set(key, value);
    }

    public static void set(String key, Object value, long timeout, TimeUnit unit) {
        operations.set(key, value, timeout, unit);
    }

    public static Boolean setIfAbsent(String key, Object value) {
        return operations.setIfAbsent(key, value);
    }

    public static Boolean setIfAbsent(String key, Object value, long timeout, TimeUnit unit) {
        return operations.setIfAbsent(key, value, timeout, unit);
    }

    public static Boolean setIfPresent(String key, Object value) {
        return operations.setIfPresent(key, value);
    }

    public static Boolean setIfPresent(String key, Object value, long timeout, TimeUnit unit) {
        return operations.setIfPresent(key, value, timeout, unit);
    }

    public static void multiSet(Map<? extends String, ?> map) {
        operations.multiSet(map);
    }

    public static Boolean multiSetIfAbsent(Map<? extends String, ?> map) {
        return operations.multiSetIfAbsent(map);
    }

    public static Object get(Object key) {
        return operations.get(key);
    }

    public static Object getAndSet(String key, Object value) {
        return operations.getAndSet(key, value);
    }

    public static List<Object> multiGet(Collection<String> keys) {
        return operations.multiGet(keys);
    }

    public static Long increment(String key) {
        return operations.increment(key);
    }

    public static Long increment(String key, long delta) {
        return operations.increment(key, delta);
    }

    public static Double increment(String key, double delta) {
        return operations.increment(key, delta);
    }

    public static Long decrement(String key) {
        return operations.decrement(key);
    }

    public static Long decrement(String key, long delta) {
        return operations.decrement(key, delta);
    }

    public static Integer append(String key, String value) {
        return operations.append(key, value);
    }

    public static String get(String key, long start, long end) {
        return operations.get(key, start, end);
    }

    public static void set(String key, Object value, long offset) {
        operations.set(key, value, offset);
    }

    public static Long size(String key) {
        return operations.size(key);
    }

    public static Boolean setBit(String key, long offset, boolean value) {
        return operations.setBit(key, offset, value);
    }

    public static Boolean getBit(String key, long offset) {
        return operations.getBit(key, offset);
    }

    public static List<Long> bitField(String key, BitFieldSubCommands subCommands) {
        return operations.bitField(key, subCommands);
    }

    public static RedisOperations<String, Object> getOperations() {
        return operations.getOperations();
    }
}
