package com.example.starter.redis.util;

import org.springframework.data.redis.core.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author 王令
 * @since 2023/3/20 9:04
 */
public class RedisSetUtil {

    private static SetOperations<String, Object> operations;

    public RedisSetUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisSetUtil.operations = redisTemplate.opsForSet();
    }

    public static Long add(String key, Object... values) {
        return operations.add(key, values);
    }

    public static Long remove(String key, Object... values) {
        return operations.remove(key, values);
    }

    public static Object pop(String key) {
        return operations.pop(key);
    }

    public static List<Object> pop(String key, long count) {
        return operations.pop(key, count);
    }

    public static Boolean move(String key, Object value, String destKey) {
        return operations.move(key, value, destKey);
    }

    public static Long size(String key) {
        return operations.size(key);
    }

    public static Boolean isMember(String key, Object o) {
        return operations.isMember(key, o);
    }

    public static Set<Object> intersect(String key, String otherKey) {
        return operations.intersect(key, otherKey);
    }

    public static Set<Object> intersect(String key, Collection<String> otherKeys) {
        return operations.intersect(key, otherKeys);
    }

    public static Set<Object> intersect(Collection<String> keys) {
        return operations.intersect(keys);
    }

    public static Long intersectAndStore(String key, String otherKey, String destKey) {
        return operations.intersectAndStore(key, otherKey, destKey);
    }

    public static Long intersectAndStore(String key, Collection<String> otherKeys, String destKey) {
        return operations.intersectAndStore(key, otherKeys, destKey);
    }

    public static Long intersectAndStore(Collection<String> keys, String destKey) {
        return operations.intersectAndStore(keys, destKey);
    }

    public static Set<Object> union(String key, String otherKey) {
        return operations.union(key, otherKey);
    }

    public static Set<Object> union(String key, Collection<String> otherKeys) {
        return operations.union(key, otherKeys);
    }

    public static Set<Object> union(Collection<String> keys) {
        return operations.union(keys);
    }

    public static Long unionAndStore(String key, String otherKey, String destKey) {
        return operations.unionAndStore(key, otherKey, destKey);
    }

    public static Long unionAndStore(String key, Collection<String> otherKeys, String destKey) {
        return operations.unionAndStore(key, otherKeys, destKey);
    }

    public static Long unionAndStore(Collection<String> keys, String destKey) {
        return operations.unionAndStore(keys, destKey);
    }

    public static Set<Object> difference(String key, String otherKey) {
        return operations.difference(key, otherKey);
    }

    public static Set<Object> difference(String key, Collection<String> otherKeys) {
        return operations.difference(key, otherKeys);
    }

    public static Set<Object> difference(Collection<String> keys) {
        return operations.difference(keys);
    }

    public static Long differenceAndStore(String key, String otherKey, String destKey) {
        return operations.differenceAndStore(key, otherKey, destKey);
    }

    public static Long differenceAndStore(String key, Collection<String> otherKeys, String destKey) {
        return operations.differenceAndStore(key, otherKeys, destKey);
    }

    public static Long differenceAndStore(Collection<String> keys, String destKey) {
        return operations.differenceAndStore(keys, destKey);
    }

    public static Set<Object> members(String key) {
        return operations.members(key);
    }

    public static Object randomMember(String key) {
        return operations.randomMember(key);
    }

    public static Set<Object> distinctRandomMembers(String key, long count) {
        return operations.distinctRandomMembers(key, count);
    }

    public static List<Object> randomMembers(String key, long count) {
        return operations.randomMembers(key, count);
    }

    public static Cursor<Object> scan(String key, ScanOptions options) {
        return operations.scan(key, options);
    }

    public static RedisOperations<String, Object> getOperations() {
        return operations.getOperations();
    }
}
