package com.example.starter.redis.util;

import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author 王令
 * @since 2023/5/12 13:45
 */
public class RedisUtil {

    private static RedisTemplate<String, Object> redisTemplate;

    public RedisUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisUtil.redisTemplate = redisTemplate;
    }

    public static void expire(String key, long timeout, TimeUnit unit) {
        redisTemplate.expire(key, timeout, unit);
    }

    public static void expire(String key, Duration timeout) {
        redisTemplate.expire(key, timeout);
    }

    public static Long getExpire(String key) {
        return redisTemplate.getExpire(key);
    }

    public static Long getExpire(String key, final TimeUnit timeUnit) {
        return redisTemplate.getExpire(key, timeUnit);
    }

    public static Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    public static Long delete(Collection<String> keys) {
        return redisTemplate.delete(keys);
    }

    public static Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    public static Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }

    public static void convertAndSend(String channel, Object message) {
        redisTemplate.convertAndSend(channel, message);
    }
}
