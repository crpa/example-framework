package com.example.starter.redis.util;

import org.springframework.data.redis.core.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author 王令
 * @since 2023/3/20 9:50
 */
public class RedisHashUtil {

    private static HashOperations<String, Object, Object> operations;

    public RedisHashUtil(RedisTemplate<String, Object> redisTemplate) {
        RedisHashUtil.operations = redisTemplate.opsForHash();
    }

    public static Long delete(String key, Object... hashKeys) {
        return operations.delete(key, hashKeys);
    }

    public static Boolean hasKey(String key, Object hashKey) {
        return operations.hasKey(key, hashKey);
    }

    public static Object get(String key, Object hashKey) {
        return operations.get(key, hashKey);
    }

    public static List<Object> multiGet(String key, Collection<Object> hashKeys) {
        return operations.multiGet(key, hashKeys);
    }

    public static Long increment(String key, Object hashKey, long delta) {
        return operations.increment(key, hashKey, delta);
    }

    public static Double increment(String key, Object hashKey, double delta) {
        return operations.increment(key, hashKey, delta);
    }

    public static Set<Object> keys(String key) {
        return operations.keys(key);
    }

    public static Long lengthOfValue(String key, Object hashKey) {
        return operations.lengthOfValue(key, hashKey);
    }

    public static Long size(String key) {
        return operations.size(key);
    }

    public static void putAll(String key, Map<?, ?> m) {
        operations.putAll(key, m);
    }

    public static void put(String key, Object hashKey, Object value) {
        operations.put(key, hashKey, value);
    }

    public static Boolean putIfAbsent(String key, Object hashKey, Object value) {
        return operations.putIfAbsent(key, hashKey, value);
    }

    public static List<Object> values(String key) {
        return operations.values(key);
    }

    public static Map<Object, Object> entries(String key) {
        return operations.entries(key);
    }

    public static Cursor<Entry<Object, Object>> scan(String key, ScanOptions options) {
        return operations.scan(key, options);
    }

    public static RedisOperations<String, ?> getOperations() {
        return operations.getOperations();
    }

}
