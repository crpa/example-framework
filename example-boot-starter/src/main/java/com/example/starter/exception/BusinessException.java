package com.example.starter.exception;

import com.example.starter.constant.ResultStatus;
import org.springframework.http.HttpStatus;

/**
 * <p>
 *     自定义异常类
 * </p>
 * @author 王令
 * @since 2022/12/30 11:25
 */
public class BusinessException extends RuntimeException implements ResultStatus {

    private final Integer code;

    public BusinessException() {
        super(ResultStatus.RESULT_FAIL_MSG);
        this.code = ResultStatus.RESULT_FAIL_CODE;
    }

    public BusinessException(String message) {
        super(message);
        this.code = ResultStatus.RESULT_FAIL_CODE;
    }

    public BusinessException(HttpStatus status) {
        super(status.getReasonPhrase());
        this.code = status.value();
    }

    public BusinessException(String message, HttpStatus status) {
        super(message);
        this.code = status.value();
    }

    @Override
    public Integer getCode() {
        return this.code;
    }
}
