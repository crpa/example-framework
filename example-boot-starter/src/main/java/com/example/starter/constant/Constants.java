package com.example.starter.constant;

/**
 * @author 王令
 * @since 2023/1/15
 */
public interface Constants {

    String DATE_TIME_FORMAT_1 = "yyyy-MM-dd HH:mm:ss,SSS";

    String DATE_TIME_FORMAT_2 = "yyyy-MM-dd HH:mm:ss";

    String DATE_FORMAT_1 = "yyyy-MM-dd";

    String DATE_FORMAT_2 = "yyyy/MM/dd";

}
