package com.example.starter.constant;

import org.springframework.http.HttpStatus;

/**
 * <p>
 * 响应状态接口
 * </p>
 *
 * @author 王令
 * @since 2021-12-30
 */
public interface ResultStatus {

    Integer RESULT_OK_CODE = 20000;
    String RESULT_OK_MSG = HttpStatus.OK.getReasonPhrase();
    Integer RESULT_FAIL_CODE = 40000;
    String RESULT_FAIL_MSG = HttpStatus.BAD_REQUEST.getReasonPhrase();
    Integer RESULT_ERROR_CODE = 50000;
    String RESULT_ERROR_MSG = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();


    /**
     * 响应码
     *
     * @return 响应码
     */
    Integer getCode();

    /**
     * 响应消息
     *
     * @return 响应消息
     */
    String getMessage();
}
