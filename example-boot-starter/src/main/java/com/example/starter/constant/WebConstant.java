package com.example.starter.constant;

/**
 * @author 王令
 * @since 2022/9/11 6:46
 */
public interface WebConstant {

    /**
     * 请求来源
     */
    String FROM = "From";

    /**
     * 内部feign请求标记
     */
    String FROM_INNER = "Inner";

    String TENANT = "tenantId";

    String APPLICATION_JSON_VALUE_UTF_8 = "application/json;charset=UTF-8";
}
