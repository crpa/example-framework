package com.example.starter.constant;

/**
 * @author 王令
 * @since 2022/9/11 12:42
 */
public interface SecurityConstant {

    String AUTHORIZATION = "Authorization";

    String TOKEN = "token";

}
