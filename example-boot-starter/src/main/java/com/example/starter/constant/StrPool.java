package com.example.starter.constant;

/**
 * @author 王令
 * @since 2023/3/13
 */
public interface StrPool extends cn.hutool.core.text.StrPool {

    String AT_CLASS = "@class";

    String ASTERISK = "*";
}
