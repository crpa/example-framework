package com.example.starter.util;

import com.example.starter.constant.ResultStatus;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * <p>
 * [ok]成功响应, [fail]失败响应, [error]异常响应
 * </p>
 *
 * @author 王令
 * @since 2021-12-30
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class Response<T> implements Serializable {

    private static final long serialVersionUID = -5431344840208245994L;
    private static final String CODE_RESOLVE_EXCEPTION = "响应码解析异常";

    private Integer code;
    private String message;
    private T data;
    private Long timestamp;

    public Response(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Response(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Response(ResultStatus status) {
        this.code = status.getCode();
        this.message = status.getMessage();
    }

    public Response(ResultStatus status, T data) {
        this.code = status.getCode();
        this.message = status.getMessage();
        this.data = data;
    }

    public Response(HttpStatus status) {
        this.code = status.value() * 100;
        this.message = status.name();
    }

    public Response(HttpStatus status, String message) {
        this.code = status.value() * 100;
        this.message = message;
    }

    @NonNull
    public HttpStatus resolveCode() {
        try {
            Assert.notNull(code, CODE_RESOLVE_EXCEPTION);
            Assert.isTrue(code > 100, CODE_RESOLVE_EXCEPTION);
            HttpStatus resolve;
            if (code < 600) {
                resolve = HttpStatus.resolve(code);
                Assert.notNull(resolve, CODE_RESOLVE_EXCEPTION);
                return resolve;
            }
            int code = this.code / 100;
            resolve = HttpStatus.resolve(code);
            Assert.notNull(resolve, CODE_RESOLVE_EXCEPTION);
            return resolve;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public static <T> Response<T> ok() {
        return ok(ResultEnum.RESULT_OK);
    }

    public static <T> Response<T> ok(Integer code) {
        return ok(code, ResultEnum.RESULT_OK_MSG);
    }

    public static <T> Response<T> ok(String message) {
        return ok(ResultEnum.RESULT_OK_CODE, message);
    }

    public static <T> Response<T> ok(Integer code, String message) {
        return new Response<>(code, message);
    }

    public static <T> Response<T> ok(T data) {
        return ok(ResultEnum.RESULT_OK, data);
    }

    public static <T> Response<T> ok(Integer code, String message, T data) {
        return new Response<>(code, message, data);
    }

    public static <T> Response<T> ok(ResultStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> ok(ResultStatus status, T data) {
        return new Response<>(status, data);
    }

    public static <T> Response<T> ok(HttpStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> fail() {
        return fail(ResultEnum.RESULT_FAIL);
    }

    public static <T> Response<T> fail(Integer code) {
        return fail(code, ResultEnum.RESULT_FAIL_MSG);
    }

    public static <T> Response<T> fail(String message) {
        return fail(ResultEnum.RESULT_FAIL_CODE, message);
    }

    public static <T> Response<T> fail(Integer code, String message) {
        return new Response<>(code, message);
    }

    public static <T> Response<T> fail(T data) {
        return new Response<>(ResultEnum.RESULT_FAIL, data);
    }

    public static <T> Response<T> fail(ResultStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> fail(ResultStatus status, T data) {
        return new Response<>(status, data);
    }

    public static <T> Response<T> fail(HttpStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> fail(HttpStatus status, String message) {
        return new Response<>(status, message);
    }

    public static <T> Response<T> error() {
        return error(ResultEnum.RESULT_ERROR);
    }

    public static <T> Response<T> error(String errMsg) {
        return error(ResultEnum.RESULT_ERROR_CODE, errMsg);
    }

    public static <T> Response<T> error(Integer code, String errMsg) {
        return new Response<>(code, errMsg);
    }

    public static <T> Response<T> error(ResultStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> error(HttpStatus status) {
        return new Response<>(status);
    }

    public static <T> Response<T> call(Response<T> response) {
        return new Response<>(response.code, response.message, response.data);
    }

    @Getter
    @RequiredArgsConstructor
    enum ResultEnum implements ResultStatus {
        /**
         * 请求成功
         */
        RESULT_OK(RESULT_OK_CODE, RESULT_OK_MSG),
        /**
         * 请求失败
         */
        RESULT_FAIL(RESULT_FAIL_CODE, RESULT_FAIL_MSG),
        /**
         * 服务器异常
         */
        RESULT_ERROR(RESULT_ERROR_CODE, RESULT_ERROR_MSG);

        private final Integer code;

        private final String message;
    }
}
