package com.example.starter.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.AntPathMatcher;

import java.util.List;

/**
 * <p>
 *  AntPathMatcher工具类
 * </p>
 *
 * @author 王令
 * @since 2022/9/11 18:54
 */
public class AntPathUtil {

    private static final AntPathMatcher instance = new AntPathMatcher();

    public static Boolean match(String pattern, String path) {
        return instance.match(pattern, path);
    }

    public static Boolean matches(List<String> patterns, String path) {
        for (String pattern : patterns) {
            if (match(pattern, path)) {
                return true;
            }
        }
        return false;
    }

    public static Boolean isPattern(String path) {
        if (StrUtil.isBlank(path)) {
            return false;
        }
        return instance.isPattern(path);
    }
}
